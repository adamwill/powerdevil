# Translation of powerdevil.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sönke Dibbern <s_dibbern@web.de>, 2008, 2009, 2014.
# Manfred Wiese <m.j.wiese@web.de>, 2009, 2010, 2011, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-28 02:37+0000\n"
"PO-Revision-Date: 2014-06-05 16:25+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sönke Dibbern, Manfred Wiese"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "s_dibbern@web.de, m.j.wiese@web.de"

#: GeneralPage.cpp:240
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, kde-format
msgid "<b>Battery Levels                     </b>"
msgstr "<b>Batteriestopen                     </b>"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, fuzzy, kde-format
#| msgid "Low level:"
msgid "&Low level:"
msgstr "Siet Batteriestoop:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "Siet-Batterie-Stoop"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr "De Batterie langt bi disse Stoop bi \"Siet\" an"

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, fuzzy, kde-format
#| msgid "Critical level:"
msgid "&Critical level:"
msgstr "Kritisch Batteriestoop:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr "Kritisch-Batteriestoop"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr "De Batterie langt bi disse Stoop bi \"Kritisch\" an"

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, fuzzy, kde-format
#| msgid "At critical level:"
msgid "A&t critical level:"
msgstr "Batterie is „Kritisch“ bi:"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr ""

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, fuzzy, kde-format
#| msgid "Configure Notifications..."
msgid "Configure Notifications…"
msgstr "Bescheden instellen..."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, kde-format
msgid "Other Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr ""

#~ msgid "Do nothing"
#~ msgstr "Nix doon"

#, fuzzy
#~| msgid "Sleep"
#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "Utsetten"

#~ msgid "Hibernate"
#~ msgstr "Infreren"

#, fuzzy
#~| msgid "Shutdown"
#~ msgid "Shut down"
#~ msgstr "Utmaken"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "As dat lett löppt dat Stroomkuntrullsysteem nich.\n"
#~ "Dat lett sik över \"An- un Utmaken\" instellen."

#~ msgid "<b>Events</b>"
#~ msgstr "<b>Begeefnissen</b>"

#~ msgid ""
#~ "When this option is selected, applications will not be allowed to inhibit "
#~ "sleep when the lid is closed"
#~ msgstr ""
#~ "Is dit anmaakt, dörvt Programmen dat Utsetten nich blockeren, wenn de "
#~ "Reeknerdeckel tomaakt warrt"

#~ msgid "Never prevent an action on lid close"
#~ msgstr "Nienich en Akschoon verhöden, wenn de Reeknerdeckel tomaakt warrt"

#~ msgid "Locks screen when waking up from suspension"
#~ msgstr "Slutt den Schirm bi't Opwaken na en Slaap af"

#~ msgid "You will be asked for a password when resuming from sleep state"
#~ msgstr "Bi't Opwaken warrst Du na't Passwoort fraagt"

#~ msgid "Loc&k screen on resume"
#~ msgstr "Schirm bi't Wiedermaken afs&luten"

#~ msgid "Battery is at low level at"
#~ msgstr "Batterie is \"Siet\" bi"

#~ msgid "When battery is at critical level"
#~ msgstr "Wenn de Batterie \"kritisch\" is"
